import { Component } from '@angular/core';
import { UserServiceService } from 'src/app/service/user-service.service';
import { Location } from '@angular/common';
import { JwtService } from 'src/app/service/jwt.service';
import { User } from 'src/app/model/User';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {

  //partie atributs

  private user: any;

  private decodeToken: any;

  private userConnect: any;

  error:any;
    
  constructor(private userService: UserServiceService, private location:Location, private jwtService: JwtService){}


  //partie admin

  public ngOnInit(): void {
    this.user = this.location.getState();
    this.jwtService.saveToken(this.user[0].token);   
    this.decodeToken = this.jwtService.decodeToken(<string>this.jwtService.getToken()); 
    console.log(this.decodeToken);
    let email = this.jwtService.getEmail(this.user[0].token);
    //this.jwtService.isTokenExpired(this.user[0].token);   
    //console.log(this.jwtService.isTokenExpired(this.user[0].token));      

    this.userService.userDatas(email).subscribe({
      next:(response:any) => {         
        this.userConnect = new User(response.lastName, response.firstName, response.email);
        this.userConnect.setId(response.id);
        this.userConnect.setPassword(response.password);
        this.userConnect.setRole(response.roles);
        //console.log(this.userConnect);
          
      }, error:(err)=>{
        this.error=err.error;
        console.log(err);       
      }
    })   
  }

}
