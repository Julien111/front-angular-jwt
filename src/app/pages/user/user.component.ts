import { Component } from '@angular/core';
import { UserServiceService } from 'src/app/service/user-service.service';
import { Location } from '@angular/common';
import { JwtService } from 'src/app/service/jwt.service';
import { User } from 'src/app/model/User';
import { AuthenticationService } from 'src/app/service/auth/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent { 

  private user: any;

  private decodeToken: any;

  private userConnect: any;

  error:any;
    
  constructor(private userService: UserServiceService, private authenticationService: AuthenticationService, private location:Location, private jwtService: JwtService, private router:Router){}

  
  public ngOnInit(): void {
    this.user = this.location.getState();
    this.jwtService.saveToken(this.user[0].token);   
    this.decodeToken = this.jwtService.decodeToken(<string>this.jwtService.getToken()); 
    console.log(this.decodeToken);
    let email = this.jwtService.getEmail(this.user[0].token);
    //this.jwtService.isTokenExpired(this.user[0].token);   
    //console.log(this.jwtService.isTokenExpired(this.user[0].token));      

    this.userService.userDatas(email).subscribe({
      next:(response:any) => {         
        this.userConnect = new User(response.lastName, response.firstName, response.email);
        this.userConnect.setId(response.id);
        this.userConnect.setPassword(response.password);
        this.userConnect.setRole(response.roles);
        //console.log(this.userConnect);
          
      }, error:(err)=>{
        this.error=err.error;
        console.log(err);       
      }
    })   
  }

  logoutUser() {
    if( confirm('Etes-vous sur de vouloir vous déconnecter')){
     this.jwtService.clearData();
     this.authenticationService.logout().subscribe({
      next:(response:any) =>{
        console.log(response);
      }
     }) 
     this.router.navigateByUrl('/');
    }
  }

}
