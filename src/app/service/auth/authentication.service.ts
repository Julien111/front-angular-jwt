import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient:HttpClient){}

  url="http://localhost:8080/api/auth";

  authentication(datas: any){
    return this.httpClient.post(this.url+"/signin", datas, {  responseType: 'text' as 'json' });
  }
  
  logout(): Observable<any> {
    return this.httpClient.get('http://localhost:8080/api/auth/logout');
  }

}
