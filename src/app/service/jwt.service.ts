import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor() { }

  //classe pour enregistrer un token, le supprimer, voir s'il existe ...

  public saveToken(token: string){
    sessionStorage.setItem('token', token);
  }

  public decodeToken(token: string){
    const tokenDecode = jwt_decode(token);  
    return tokenDecode;  
  }

  public getToken():string | null{
    return sessionStorage.getItem('token');
  }

  public isTokenExpired(token: string){
    const tokenDecode: any = jwt_decode(token);    
    return (Math.floor((new Date).getTime() / 1000)) >= tokenDecode.exp;
  }

  public getEmail(token: string){
    const tokenDecode: any = jwt_decode(token); 
    const userInfos = tokenDecode.sub;
    return userInfos;
  }

  public removeToken(key: string){
    sessionStorage.removeItem(key);
  }

  public clearData(){
    sessionStorage.clear();
  }

}
