import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../model/User';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private client: HttpClient) {}

  addUser(user: User): Observable<User> {
    return this.client.post<User>(
      `http://localhost:8080/api/auth/signup`,
      user
    );
  }

  userDatas(email: string) {  
    return this.client.post<User>(
      `http://localhost:8080/api/auth/user/find`, email    
    );
  }
  

}
