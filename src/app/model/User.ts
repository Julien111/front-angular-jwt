export class User{

    private id: number = 0;    
    private lastName: string;   
    private firstName: string;    
    private email: string;
    private password: string = '';
    private role: any = []; 
       
    constructor(lastName: string, firstName: string, email: string){
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
    }

    public getId(): number {
        return this.id;
    }
    public setId(value: number) {
        this.id = value;
    }

    public getLastName(): string {
        return this.lastName;
    }
    public setLastName(value: string) {
        this.lastName = value;
    }

    public getFirstName(): string {
        return this.firstName;
    }
    public setFirstName(value: string) {
        this.firstName = value;
    }

    public getEmail(): string {
        return this.email;
    }
    public setEmail(value: string) {
        this.email = value;
    }

    public getPassword(): string {
        return this.password;
    }
    public setPassword(value: string) {
        this.password = value;
    }

    public getRole(): any {
        return this.role;
    }
    public setRole(value: any) {
        this.role = value;
    }    

}