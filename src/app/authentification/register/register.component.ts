import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/User';
import { HttpErrorResponse } from '@angular/common/http';
import { UserServiceService } from 'src/app/service/user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  //error attribute
  errorLastName: string = '';
  errorFirstName: string = '';
  errorEmail: string = '';
  errorPassword: string = '';
  errormessage: string = '';

  //regex email
  regex = new RegExp('[a-z0-9-.]+@[a-z]+.[a-z]{2,3}');

  constructor(   
    private router: Router,
    private userService: UserServiceService
  ) {}

  validateFormFieldAndSave(formLogin: NgForm) {
    let infos = formLogin.value;
    //console.log(infos);

    if (infos.lastName == '' || infos.lastName == null) {
      this.errorLastName = 'Field empty';
    } else if (infos.firstName == '' || infos.firstName == null) {
      this.errorFirstName = 'Field empty';
    } else if (
      infos.email == '' ||
      infos.email == null ||
      this.regex.test(infos.email) == false
    ) {
      this.errorEmail = 'Field empty';
    } else if (
      infos.password == '' ||
      infos.password == null ||
      infos.password.length < 8
    ) {
      this.errorPassword =
        'Password is empty, invalidate, not 8 caracters';
    } 
    else {
      let user = new User(
        infos.lastName,
        infos.firstName,
        infos.email          
      );
      user.setPassword(infos.password);
      user.setRole('ROLE_USER');
      console.log(user);
      //ajouter alert() pour confirmer l'inscription
      this.userService.addUser(user).subscribe({
        next: () => {
          this.router.navigate(['/login']);
          alert('Votre inscription a été confirmée.');
        },
        error: (err) => {
          this.gestionErreurs(err.error);
        },
      });
    }
  }
  //gestion des erreurs
  gestionErreurs(message: string): void {
    this.errormessage = message;
    this.redirectFormRegister();
  }

  //méthode de redirection
  redirectFormRegister() {
    this.router.navigate(['/register']);
  }

  redirectHome() {
    this.router.navigate(['/']);
  }

}
