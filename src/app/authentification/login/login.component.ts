import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/service/auth/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private authenticationService:AuthenticationService, private router:Router){

  }
  error:any;

  authentication(datas: any){
  }

  auth(datas:NgForm){
    
    this.authenticationService.authentication(datas.value).subscribe({
      next:(response:any) => {      
        let dataUserJson = JSON.parse(response);        
        alert("You are connect !");
        //gestion des rôles utilisateurs        
        if(dataUserJson[0].roles.includes("ROLE_ADMIN") && dataUserJson[0].roles.includes("ROLE_USER")){
          console.log("partie admin");
          this.router.navigateByUrl("/admin", { state: dataUserJson });
        }
        else if(dataUserJson[0].roles.includes("ROLE_MODERATOR") && dataUserJson[0].roles.includes("ROLE_USER")){
          console.log("partie moderator");
          this.router.navigateByUrl("/moderator", { state: dataUserJson });
          console.log(dataUserJson[0].token);
        }
        else if(dataUserJson[0].roles.length == 1 && dataUserJson[0].roles.includes("ROLE_USER")){
          this.router.navigateByUrl("/user", { state: dataUserJson });
        }
        else{
          console.log("pas de rôle attribué");
        }
                
      }, error:(err)=>{
        this.error=err.error;
        console.log(err);      
        datas.reset();
      }
    })
  }

}
