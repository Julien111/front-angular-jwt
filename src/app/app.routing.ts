import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./authentification/login/login.component";
import { RegisterComponent } from "./authentification/register/register.component";
import { HomeComponentComponent } from "./home/home-component/home-component.component";
import { UserComponent } from "./pages/user/user.component";
import { ModeratorComponent } from "./pages/moderator/moderator.component";
import { AdminComponent } from "./pages/admin/admin.component";


const myRoutes: Routes = [
    { path: '', component: HomeComponentComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent },
    { path: 'user', component: UserComponent },
    { path: 'moderator', component: ModeratorComponent },
    { path: 'admin', component: AdminComponent },
];


export let PROJET_ROUTING = RouterModule.forRoot(myRoutes);